package com.msk.app.models.service;

import java.util.List;

import com.msk.app.models.entity.Persona;

public interface IPersonaService {
	
	public List<Persona> findAll();
	
	public Persona findOne(Long id);
	
	public Persona save(Persona persona);
	
	public void delete(Long id);

}
