package com.msk.app.models.service;

import java.util.List;

import com.msk.app.models.entity.Usuario;

public interface IServiceUsuario {
	
	public Usuario findOne(Long id);
	
	public List<Usuario> findAll();

	public Usuario save(Usuario usuario);
	
	public void delete(Long id);
}
