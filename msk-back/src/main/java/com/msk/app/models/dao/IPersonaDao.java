package com.msk.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.msk.app.models.entity.Persona;

public interface IPersonaDao extends CrudRepository<Persona, Long> {

}
