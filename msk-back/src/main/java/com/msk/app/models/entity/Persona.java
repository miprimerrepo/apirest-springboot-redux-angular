package com.msk.app.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "personas")
public class Persona implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 25, message = "el tamaño tiene que estar entre 4 y 25")
	@Column(nullable = false)
	private String nombre;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 30, message = "el tamaño tiene que estar entre 4 y 30")
	@Column(nullable = false)
	private String apellido;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 50, message = "el tamaño tiene que estar entre 4 y 50")
	@Email(message = "Formato Invalido")
	@Column(nullable = false,unique = true)
	private String email;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 20, message = "el tamaño tiene que estar entre 4 y 20")
	@Column(nullable = false)
	private String telefono;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 150, message = "el tamaño tiene que estar entre 4 y 150")
	@Column(nullable = false)
	private String direccion;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 20, message = "el tamaño tiene que estar entre 4 y 20")
	@Column(nullable = false)
	private String nacionalidad;

	@NotEmpty(message = "no puede estar vacío")
	@Size(min = 4, max = 250, message = "el tamaño tiene que estar entre 4 y 250")
	@Column(nullable = false)
	private String foto;

	@NotNull(message = "no puede estar vacío")
	@Column(name = "fecha_nac")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date fechaNac;
	

	
	private static final long serialVersionUID = 1L;

}
