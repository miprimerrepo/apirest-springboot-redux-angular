package com.msk.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.msk.app.models.entity.Persona;
import com.msk.app.models.service.IPersonaService;
import com.msk.app.models.service.IUploadFileService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class PersonaController {

	@Autowired
	private IPersonaService personaService;

	/*------------------------------Buscar Todos----------------------------------------------------------------------------*/
	@GetMapping("/personas")
	public List<Persona> index() {
		return personaService.findAll();
	}

	/*------------------------------Buscar 1---------------------------------------------------------------------------------*/
	@GetMapping("/personas/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Persona persona = null;
		Map<String, Object> response = new HashMap<>();

		try {
			persona = personaService.findOne(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (persona == null) {
			response.put("mensaje",
					"La Persona con el ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Persona>(persona, HttpStatus.OK);

	}

	/*----------------------------- Guardar  -------------------------------------------------------------------------------*/
	@PostMapping("/personas")
	public ResponseEntity<?> create(@Valid @RequestBody Persona persona, BindingResult result) {

		Persona personaNew = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			personaNew = personaService.save(persona);
		} catch (DataIntegrityViolationException e) {
			String[] campo = e.getMostSpecificCause().getMessage().split("'");
			String campo1 = campo[1];
			response.put("mensaje", "Error! Datos Duplicados");
			response.put("error", campo1 + " Ya Existe en la Base de Datos");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La Persona ha sido creada con éxito!");
		response.put("persona", personaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/*---------------------------------------------------Update-------------------------------------------------------------------------*/
	@PutMapping("/personas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Persona persona, BindingResult result, @PathVariable Long id) {

		Persona personaActual = personaService.findOne(id);

		Persona personaUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (personaActual == null) {
			response.put("mensaje", "Error: no se pudo editar, la Persona con ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			personaActual.setNombre(persona.getNombre());
			personaActual.setApellido(persona.getApellido());
			personaActual.setEmail(persona.getEmail());
			personaActual.setTelefono(persona.getTelefono());
			personaActual.setDireccion(persona.getDireccion());
			personaActual.setNacionalidad(persona.getNacionalidad());
			personaActual.setFoto(persona.getFoto());
			personaActual.setFechaNac(persona.getFechaNac());

			personaUpdated = personaService.save(personaActual);

		} catch (DataIntegrityViolationException e) {
			String[] campo = e.getMostSpecificCause().getMessage().split("'");
			String campo1 = campo[1];
			response.put("mensaje", "Error! Datos Duplicados");
			response.put("error", campo1 + " Ya Existe en la Base de Datos");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al Intentar Actualizar los Datos en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Datos Actualizados con éxito!");
		response.put("persona", personaUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/*-------------------------------------------Delete---------------------------------------------------------------------*/
	@DeleteMapping("/personas/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		Map<String, Object> response = new HashMap<>();

		try {

			Persona persona = personaService.findOne(id);
			String nombreFotoAnterior = persona.getFoto();
			uploadService.eliminar(nombreFotoAnterior);

			personaService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar Los Datos de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Datos Eliminados con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	/*-----------------------------------------Foto---------------------------------------------------------------------------------*/
	@Autowired
	private IUploadFileService uploadService;

	/*-----------------------------------------------------subir----------------------------------*/
	@PostMapping("/personas/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {

		Map<String, Object> response = new HashMap<>();

		Persona persona = personaService.findOne(id);

		if (!archivo.isEmpty()) {
			String nombreArchivo = null;
			try {
				nombreArchivo = uploadService.copiar(archivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al subir la imagen del cliente");
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			String nombreFotoAnterior = persona.getFoto();
			uploadService.eliminar(nombreFotoAnterior);
			persona.setFoto(nombreArchivo);
			personaService.save(persona);
			response.put("persona", persona);
			response.put("mensaje", "Has subido correctamente la imagen: " + nombreArchivo);
		}

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	/*------------------------------ver---------------------------------------------------------*/

	@GetMapping("/uploads/img/{nombreFoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto) {

		Resource recurso = null;

		try {
			recurso = uploadService.cargar(nombreFoto);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}

}
