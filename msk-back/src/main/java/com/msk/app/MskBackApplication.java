package com.msk.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MskBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MskBackApplication.class, args);
	}

}
