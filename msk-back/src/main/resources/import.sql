INSERT INTO personas (nombre,apellido,email,telefono,direccion,nacionalidad,foto,fecha_nac) VALUES ('Gerson','Aponte','3xor18@gmail.com','54666','barinas','Venezuela','null','2018-01-01');
INSERT INTO personas (nombre,apellido,email,telefono,direccion,nacionalidad,foto,fecha_nac) VALUES ('Monica','Escorcha','Monica@gmail.com','66422','Maracay','Venezuela','null','2016-02-02');
INSERT INTO personas (nombre,apellido,email,telefono,direccion,nacionalidad,foto,fecha_nac) VALUES ('Nikola','Tesla','tesla@gmail.com','0001','Alemania','Alemania','null','2018-01-01');
INSERT INTO personas (nombre,apellido,email,telefono,direccion,nacionalidad,foto,fecha_nac) VALUES ('Scott','Sommers','Scoot@gmail.com','0911','USA','GRINGO','null','2018-01-01');

INSERT INTO `usuarios` (username, password, enabled,personas_id) VALUES ('3xor','$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq',1,1);
INSERT INTO `usuarios` (username, password, enabled,personas_id) VALUES ('admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK',1,2);

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);